//
//  playerTry.swift
//  ChristmasRadioAdventCalendar
//
//  Created by Andrea Cascella on 15/12/2019.
//  Copyright © 2019 Andrea Cascella. All rights reserved.
//

import UIKit

class RadioPlayerView: UIView {

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    func setShadows(){
        self.layer.cornerRadius = 20
        self.layer.shadowColor = UIColor.black.withAlphaComponent(0.3).cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowOpacity = 1
        self.layer.masksToBounds = false
    }
    
}
