//
//  Radio.swift
//  ChristmasRadioAdventCalendar
//
//  Created by Andrea Cascella on 14/12/2019.
//  Copyright © 2019 Andrea Cascella. All rights reserved.
//

import Foundation
import UIKit

struct Radio {
    var day: Int
    var name: String
    var url: String
    var icon: String
    
    var date: String
    var unlocked: Bool
    var selected: Bool
    
    func unlock(){}
    
    func select(){}
}
