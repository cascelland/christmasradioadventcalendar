//
//  ViewController.swift
//  ChristmasRadioAdventCalendar
//
//  Created by Andrea Cascella on 14/12/2019.
//  Copyright © 2019 Andrea Cascella. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    
    
    
    @IBOutlet weak var albumCover: UIImageView!
    @IBOutlet weak var trackLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var radioNameLabel: UILabel!
    @IBOutlet weak var radioPlayerView: RadioPlayerView!
    @IBOutlet weak var radioCollectionView: UICollectionView!
    
    
    @IBAction func playButtonPressed(_ sender: Any) {
        
        playButton.superview?.layer.shadowOpacity = 1
        playButton.superview?.layer.position.y -= 3
        
        
        switch player.playbackState{
        case .playing:
            do {
                self.player.pause()
                playButton.imageView?.image = UIImage(named: "pauseButton")
                
                
            }
            
        case .paused:
            self.player.play()
        case .stopped:
            print("")
        }
    }
    
    @IBAction func playButtonIsHeld(_ sender: Any) {
        playButton.superview?.layer.shadowOpacity = 0
        playButton.superview?.layer.position.y += 3
        
    }
    
    let player = FRadioPlayer.shared
    
    let reuseIdentifier = "RadioCell"
    
    var radios: [Radio] = [
        
        Radio(day: 1, name: "Christmas FM North Pole", url: "https://ice31.securenetsystems.net/XMAS22", icon: "00", date: "01-12-2019", unlocked: false, selected: false),
        
        Radio(day: 2, name: "Jingle Bell Radio", url: "http://ca.radioboss.fm:8073/stream", icon: "01", date: "02-12-2019", unlocked: false, selected: false),
        
        Radio(day: 3, name: "Radio Natale", url: "https://streaming.radiostreamlive.com/radionatale_devices?token=%3C?%20echo%20rand%20(1,200000);%20?%3E", icon: "02", date: "03-12-2019", unlocked: false, selected: false),
        
        Radio(day: 4, name: "Merry Christmas", url: "http://tuner.m1.fm/M1-XMAS.mp3", icon: "03", date: "04-12-2019", unlocked: false, selected: false),
        
        Radio(day: 5, name: "Radio 105 Xmas Radio", url: "https://icecast.unitedradio.it/um025.mp3", icon: "04", date: "05-12-2019", unlocked: false, selected: false),
        
        Radio(day: 6, name: "Radio Santa Claus", url: "https://streaming.radiostreamlive.com/radiosantaclaus_devices?token=%3C?%20echo%20rand%20(1,200000);%20?%3E", icon: "05", date: "06-12-2019", unlocked: false, selected: false),
        
        Radio(day: 7, name: "181 FM XMas Fun", url: "http://listen.181fm.com:7080/181-xfun_128k.mp3", icon: "06", date: "07-12-2019", unlocked: false, selected: false),
        
        Radio(day: 8, name: "Jazz Radio - Xmas Jazz", url: "http://jzr-events-01.ice.infomaniak.ch/jzr-events-01.aac", icon: "07", date: "08-12-2019", unlocked: false, selected: false),
        
        Radio(day: 9, name: "XmasMelody", url: "http://s7.viastreaming.net:7000/stream", icon: "08", date: "09-12-2019", unlocked: false, selected: false),
        
        Radio(day: 10, name: "Forever Christmas", url: "http://70.38.12.44:8244/stream/1/", icon: "09", date: "10-12-2019", unlocked: false, selected: false),
        
        Radio(day: 11, name: "80s80s Christmas", url: "http://80s80s.hoerradar.de/80s80s-event01-mp3-mq", icon: "10", date: "11-12-2019", unlocked: false, selected: false),
        
        Radio(day: 12, name: "Radio BOB! BOBs Christmas Rock", url: "http://bob.hoerradar.de/radiobob-event01-mp3-mq", icon: "11", date: "12-12-2019", unlocked: false, selected: false),
        
        Radio(day: 13, name: "100% Black Soul Christmas", url: "http://xmasfm.hoerradar.de/xmasfm-black-mp3-mq", icon: "12", date: "13-12-2019", unlocked: false, selected: false),
        
        Radio(day: 14, name: "90s90s Christmas", url: "http://90s90s.hoerradar.de/90s90s-xmas-mp3-hq", icon: "13", date: "14-12-2019", unlocked: false, selected: false),
        
        Radio(day: 15, name: "Starlite Radio", url: "http://centova.radioservers.biz:8021/stream/1/", icon: "14", date: "15-12-2019", unlocked: false, selected: false),
        
        Radio(day: 16, name: "Tinsel & Tunes", url: "http://tinsel.purestream.net:9952/;stream.mp3", icon: "15", date: "16-12-2019", unlocked: false, selected: false),
        
        Radio(day: 17, name: "J-Pop Christmas Radio", url: "http://agnes.torontocast.com:8087/stream/1/", icon: "16", date: "17-12-2019", unlocked: false, selected: false),
        
        Radio(day: 18, name: "The Christmas Lite", url: "http://naxos.cdnstream.com/1304_64", icon: "17", date: "18-12-2019", unlocked: false, selected: false),
        
        Radio(day: 19, name: "American Christmas", url: "https://str2b.openstream.co/874?aw_0_1st.stationid=3668&aw_0_1st.publisherId=898&aw_0_1st.serverId=str2b", icon: "18", date: "19-12-2019", unlocked: false, selected: false),
        
        Radio(day: 20, name: "SomaFM - The Christmas Lounge", url: "http://ice2.somafm.com/christmas-128-mp3", icon: "19", date: "20-12-2019", unlocked: false, selected: false),
        
        Radio(day: 21, name: "Nostalgie Noël", url: "http://185.52.127.157/fr/55310/mp3_128.mp3", icon: "20", date: "21-12-2019", unlocked: false, selected: false),
        
        Radio(day: 22, name: "Life Radio Xmas", url: "http://liferadio.stream.kapper.net:8011/xmas", icon: "21", date: "22-12-2019", unlocked: false, selected: false),
        
        Radio(day: 23, name: "ON Christmas", url: "http://0n-christmas.radionetz.de/0n-christmas.mp3", icon: "22", date: "23-12-2019", unlocked: false, selected: false),
        
        Radio(day: 24, name: "Christmas 365 - Santa's radio", url: "http://174.36.206.197:7038/stream/1/", icon: "23", date: "24-12-2019", unlocked: false, selected: false)
        
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        radioCollectionView.delegate = self
        radioCollectionView.dataSource = self
        radioCollectionView.backgroundColor = UIColor.clear
        player.delegate = self
        radioPlayerView.setShadows()
        
        let buttonCircle = playButton.superview
//        playButton.contentEdgeInsets = UIEdgeInsets(top: 30, left: 30, bottom: 30, right: 30)

        buttonCircle!.frame.size = albumCover.frame.size
        buttonCircle!.layer.cornerRadius = min(albumCover.frame.width / 2.0, albumCover.frame.height / 2.0)
        buttonCircle?.layer.shadowPath = UIBezierPath(ovalIn: buttonCircle!.bounds).cgPath
        buttonCircle?.layer.shadowColor = UIColor.black.withAlphaComponent(0.3).cgColor
        buttonCircle?.layer.shadowOffset = CGSize(width: 0, height: 3)
        buttonCircle?.layer.shadowOpacity = 1
        buttonCircle?.layer.masksToBounds = false
        
        
        //        playButton.center.x = playButton.superview!.center.x
        //        playButton.center.y = playButton.superview!.center.y
        //        playButton.imageView?.center = playButton.superview!.center
        
        
        
        
        
        
    }
    
    func drawViews(){
        
    }
    
    
    
    
}



extension ViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return radios.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! RadioCollectionViewCell
        
        
        cell.update(with: radios[indexPath.row])
        
        
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let radioDate = dateFormatter.date(from: radios[indexPath.row].date)!
        
        if radios[indexPath.row].unlocked == true{
            if radios[indexPath.row].selected == false{
                if let i = radios.firstIndex(where:({$0.selected == true})){
                    radios[i].selected = false
                    if let cell=collectionView.cellForItem(at: [0, i]){
                        cell.contentView.viewWithTag(001)?.layer.shadowOpacity = 0.3
                        cell.contentView.viewWithTag(001)?.layer.position.y = cell.contentView.layer.position.y
                        cell.contentView.viewWithTag(001)?.layer.backgroundColor = UIColor(red: 0.51, green: 0, blue: 0, alpha: 1).cgColor
                    }
                }
                radios[indexPath.row].selected = true
                if let cell=collectionView.cellForItem(at: indexPath){
                    cell.contentView.viewWithTag(001)?.layer.shadowOpacity = 0
                    cell.contentView.viewWithTag(001)?.layer.position.y = cell.contentView.layer.position.y + 3
                    cell.contentView.viewWithTag(001)?.layer.backgroundColor = UIColor(red: 0.43, green: 0, blue: 0, alpha: 1).cgColor
                    player.radioURL = URL(string: radios[indexPath.row].url)
                    radioNameLabel.text = radios[indexPath.row].name
                    player.play()
                    
                    
                    
                }
                
                
                
                
                
                
                
                
            }
        } else {
            
            let today = dateFormatter.date(from: dateFormatter.string(from: Date()))
            if radioDate <= today!{
                let cell = collectionView.cellForItem(at: indexPath) as! RadioCollectionViewCell
                    
                radios[indexPath.row].unlocked = true
//                radios[indexPath.row].selected = true
//                cell.contentView.viewWithTag(001)?.layer.shadowOpacity = 0
//                cell.contentView.viewWithTag(001)?.layer.position.y = cell.contentView.layer.position.y + 3
                player.radioURL = URL(string: radios[indexPath.row].url)
                radioNameLabel.text = radios[indexPath.row].name
                cell.update(with: radios[indexPath.row])
                player.play()
                
                
                
                
            }
        }
        
    }
    
    
    
}

extension ViewController: FRadioPlayerDelegate{
    func radioPlayer(_ player: FRadioPlayer, playerStateDidChange state: FRadioPlayerState) {
    }
    
    func radioPlayer(_ player: FRadioPlayer, playbackStateDidChange state: FRadioPlaybackState) {
        
        print(state.description)
        
        switch state {
        case .playing:
            
            self.playButton.imageView?.image = UIImage(named: "pauseButton")
        case .paused:
            self.playButton.imageView?.image = UIImage(named: "playButton")
        case .stopped:
            self.playButton.imageView?.image = UIImage(named: "playButton")
            
        }
        
    }
    
    func radioPlayer(_ player: FRadioPlayer, metadataDidChange artistName: String?, trackName: String?) {
        
        if player.radioURL == URL(string: radios[4].url){
            self.artistLabel.text = String(artistName?.split(separator: "~")[0] ?? " - ")
            self.trackLabel.text = String(artistName?.split(separator: "~")[1] ?? " - ")
        } else {
            
            self.artistLabel.text = artistName ?? "-"
            self.trackLabel.text = trackName ?? "-"
        }
    }
    
    func radioPlayer(_ player: FRadioPlayer, artworkDidChange artworkURL: URL?) {
        //        sleep(3)
        //        albumCover.load(url: artworkURL ?? URL(fileURLWithPath: Bundle.main.path(forResource: "00", ofType: "png")!))
        if let i = radios.firstIndex(where: {$0.url == player.radioURL?.absoluteString}){
            print(radios[i].icon)
            albumCover.image = UIImage(named: radios[i].icon)
            albumCover.layer.cornerRadius = CGFloat(min(albumCover.frame.width/2, albumCover.frame.height/2))
            albumCover.addDashedBorder()
            
        }
        if let myCover = artworkURL{
            albumCover.load(url: myCover)
        }
        
    }
}




extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}



