//
//  RadioCollectionViewCell.swift
//  ChristmasRadioAdventCalendar
//
//  Created by Andrea Cascella on 14/12/2019.
//  Copyright © 2019 Andrea Cascella. All rights reserved.
//

import UIKit

class RadioCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var radioCellView: UIView!
    
    @IBOutlet weak var radioDayLabel: UILabel!
    
    let dateFormatter = DateFormatter()
    
    
    
    func update(with radio: Radio){
        
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let radioDate = dateFormatter.date(from: radio.date)
        
        radioCellView.layer.cornerRadius = min(radioCellView.frame.width, radioCellView.frame.height)/2
        
        let centerView = UIView()
        centerView.translatesAutoresizingMaskIntoConstraints = false
        centerView.backgroundColor = UIColor(red:0.51, green:0.00, blue:0.00, alpha:1.0)
        centerView.frame = radioCellView.frame
        centerView.layer.cornerRadius = radioCellView.layer.cornerRadius
        centerView.tag = 001
        
        
        
        
        //        radioCellView.clipsToBounds = true
        
        if radioDate! <= Date() {
            
            radioCellView.addDashedBorder()
            radioDayLabel.text = String(radio.day)
            
            if radio.selected == false  {
                //                    print("najnajnd")
                centerView.layer.shadowPath = UIBezierPath(ovalIn: radioCellView.bounds).cgPath
                centerView.layer.shadowColor = UIColor.black.withAlphaComponent(0.3).cgColor
                centerView.layer.shadowOffset = CGSize(width: 0, height: 3)
                centerView.layer.shadowOpacity = 0.3
                centerView.layer.position.y = radioCellView.layer.position.y
                centerView.layer.masksToBounds = false
                
            }
            
            if radio.unlocked == true {
                radioDayLabel.removeFromSuperview()
                radioCellView.layer.sublayers?.removeAll()
                radioCellView.addSubview(centerView)
                let snowflake =  UIImageView(image: UIImage(named: radio.icon))
                snowflake.frame.size = CGSize(width: centerView.frame.width/1.5, height: centerView.frame.height/1.5)
                snowflake.center = CGPoint(x: centerView.center.x, y: centerView.center.y)
                centerView.addSubview(snowflake)
                centerView.bringSubviewToFront(snowflake)
                
                
                
                
                
                
                
            }
        } else {
            
            radioDayLabel.text = String(radio.day)
            
        }
        
    }
    
    
    
    
    
    
}



extension UIView {
    
    
    func addDashedBorder() {
        let color = UIColor.white.cgColor
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 3
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [3,10]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: self.layer.cornerRadius).cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
    
    
    
}

